﻿using UnityEngine;
using System.Collections;

public class FiringMahLazor : MonoBehaviour {

	public GameObject lazors;
	public float forwardOffset;
	public float minSpeed = 50.0f;
	public float maxSpeed = 100.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FireMahLazor (float strength)
	{
		GameObject lazorClone;
		if(lazors != null ) //&& lazors.rigidbody != null)
		{
			lazorClone = (GameObject)Instantiate(lazors, transform.position + transform.forward * forwardOffset, this.gameObject.transform.rotation);
			lazorClone.rigidbody.AddForce(lazorClone.transform.forward * Mathf.Lerp(minSpeed, maxSpeed, strength));
			//TODO: couple strength with size//effects and damage!
			
		}
	}
}
