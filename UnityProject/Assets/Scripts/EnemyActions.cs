﻿using UnityEngine;
using System.Collections;

public class EnemyActions : MonoBehaviour {



	
	

	// Use this for initialization
	void Start () 
	{
		BaseEnemy thisInputHandler = this.gameObject.GetComponent<BaseEnemy>();
		if(thisInputHandler != null)
		{
			thisInputHandler.MoveHorizontal += MoveHorizontalListener;
			thisInputHandler.MoveVertical += MoveVerticalListener;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		// rotate slowly towards set rotation/direction
		//this.gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, rotatingTo, 1.0f);
		
		//rotate /angle back to default position when bankLastFrame == false
		
		// update unrelated stuff
		// handle collision events (EXPLOSIOOONS (effects component?))
	}
	
	void FixedUpdate()
	{
		

		
	}
	
	// Move up / down
	void MoveVerticalListener (GameObject e, float strength)
	{
		// just add some force yo. or set accell
	}
	
	// move left/right
	void MoveHorizontalListener(GameObject e, float strength)
	{
		// add force or set accel
	}
	
}
