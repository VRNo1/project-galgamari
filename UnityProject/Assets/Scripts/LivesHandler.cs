﻿using UnityEngine;
using System.Collections;

public class LivesHandler : MonoBehaviour 
{
	GameObject playerShip;
	//bool playerActive = false;
	float startLives;
	GUIText livesText;

	// Use this for initialization
	void Start () 
	{
		livesText = this.gameObject.GetComponent<GUIText>();

		playerShip = GameObject.FindGameObjectWithTag("Player");
		if(playerShip == null)
			Debug.LogError("No player found, lives will be ignored.");
		else
		{
			playerShip.GetComponent<DestructionHandler>().LivesChange += LifeChanger;
			startLives = playerShip.GetComponent<DestructionHandler>().startHealth;
			this.gameObject.GetComponent<GUIText>().text = "Shields at: 100%";
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void LifeChanger(GameObject e, float newLife)
	{
		newLife = newLife/startLives * 100; // TODO make real percentage;
		if(e.tag == "Player")//ignore if not sent by player. Shouldn't happen though.
			livesText.text = "Shields at: "  + newLife + "%";
	
			if(newLife <= 2) // turn red
				livesText.color = Color.red;
			else if(newLife <= 4)
				livesText.color = Color.yellow;
			else
				livesText.color = Color.green;
	}
}
