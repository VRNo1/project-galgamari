﻿using UnityEngine;
using System.Collections;

public enum ButtonStates
{
	isRelease,
	isNewPress,
	isHeld,
	isDoublePress
};

public class ShipInputHandler : MonoBehaviour 
{

	// handles the input and tells the physics to do something about it.
	// Move the ship
	// barrel roll
	// fire the LAZORS //true: held, false: not held
	public float fireChargeTime = 1.0f;
	public float doublePressPeriod = 0.2f;

	float currentCharge = 0.0f;
	bool previousMovement = false;

	float doubleLeft = 0.0f;
	float doubleRight = 0.0f;

	//declare inputeventhandler
	// sending object, strength of movement
	public delegate void EventHandler(GameObject e);
	public delegate void InputHandler(GameObject e, float strength);
	public delegate void ButtonHandler(GameObject e, ButtonStates buttonState);

	public event InputHandler MoveHorizontal;
	public event InputHandler MoveVertical;
	public event InputHandler Boost;
	public event ButtonHandler HardLeft;
	public event ButtonHandler HardRight;
	public event EventHandler MovementRelease;

	public event InputHandler FireLazors;

	bool isInverted = false;

	// Use this for initialization
	void Start () 
	{
	
		//initialize the values and stuff.
		currentCharge = 0.0f;

	}
	
	// Update is called once per frame
	void Update () 
	{
		//get the input

		// ### normal movement ###
		bool noMovement = true;
		// handle it (tell the physics to move, the soundmanager to play a sound or the game to instantiate a LAZERBEAM)
		float axisVal = Input.GetAxis("Horizontal");
		if(Mathf.Abs(axisVal) >= 0.1)
		{
			OnMoveHorizontal(axisVal);
			noMovement = false;
		}

		axisVal = Input.GetAxis("Vertical");
		if(Mathf.Abs(axisVal) >= 0.1)
		{
			if(isInverted)
				axisVal = -axisVal;
			OnMoveVertical(axisVal);
			noMovement = false;
		}

		axisVal = Input.GetAxis("Boost");
			OnBoost(-axisVal);

		if(noMovement && previousMovement)
			OnMovementRelease();
		
		previousMovement = noMovement;

		/// ### HARD LEFT #####
		if(Input.GetButton("HardLeft"))
		{
			if(doubleLeft > 0.0f)
			{
				doubleLeft = 0.0f;
				OnHardLeft(ButtonStates.isDoublePress);
			}
			else
				OnHardLeft(ButtonStates.isHeld);
		}
		else if(Input.GetButtonUp("HardLeft"))
		{
			OnHardLeft(ButtonStates.isRelease);
			doubleLeft = doublePressPeriod;

		}
		if(doubleLeft > 0.0f)
			doubleLeft -= Time.deltaTime;

		/// #### HARD RIGHT ####
		if(Input.GetButton("HardRight"))
		{
			if(doubleRight > 0.0f)
			{
				OnHardRight(ButtonStates.isDoublePress);
				doubleRight = 0.0f;
			}
			else
				OnHardRight(ButtonStates.isHeld);
		}
		else if(Input.GetButtonUp("HardRight"))
		{
			OnHardRight(ButtonStates.isRelease);
			doubleRight = doublePressPeriod;
		}

		if(doubleRight > 0.0f)
			doubleRight -= Time.deltaTime;

		/// ### FIRE THE LAZORS ###
		if(Input.GetButton("Fire")) // is pressing the fire button.
		{
			if(currentCharge < 1.0) 
			{
				//increase 
				currentCharge +=  Time.deltaTime / fireChargeTime;
			}

		}
		else if(currentCharge > 0.0f) //button has been released and lazers are charged!
		{
			OnFire(currentCharge);

			//reset
			currentCharge = 0.0f; // reset charge.
		}

		if(Input.GetButton("Reset")) //pressed the reset button.
		{
			Application.LoadLevel("MainMenu");
		}

		if(Input.GetButton("Inverted"))
		{
			isInverted = !isInverted;
		}
	
		// raise events!
		// move raising events into extra virtual classes if necessar
		//DO NOT DO THESE THINGS YOURSELF!


	}

	void OnMoveHorizontal(float strength)
	{
		if(MoveHorizontal != null)
			MoveHorizontal(this.gameObject, strength);
	}

	void OnMoveVertical(float strength)
	{
		if(MoveVertical != null)
			MoveVertical(this.gameObject, strength);
	}

	void OnBoost(float strength)
	{
		if(Boost != null)
			Boost(this.gameObject, strength);
	}

	void OnHardRight (ButtonStates currentState)
	{
		if(HardRight != null)
			HardRight(this.gameObject, currentState);
	}

	void OnHardLeft (ButtonStates currentState)
	{
		if(HardLeft != null)
			HardLeft(this.gameObject, currentState);
	}

	void OnMovementRelease()
	{
		if(MovementRelease!=null)
			MovementRelease(this.gameObject);
	}
		
	void OnFire(float strength)
	{
		if(FireLazors != null)
			FireLazors(this.gameObject, Mathf.Min(strength, 1.0f));
	}
		
}
