﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {
	public Transform target;
	public float depthDistance, rate, height;

	// TODO: make updateable centerposition to accomodate level changes / paths.
	// better yet: follow a curve / path 
	// smooth interpolate between main points of the path

	// TODO: get from level dimensions
	[Range (1,10)]
	public float levelWidth = 10.0f;
	[Range (1,10)]
	public float levelHeight = 10.0f;
	
	void Start () 
	{
	}
	
	void Update () {

		if(target != null)
			transform.position = new Vector3(Mathf.Lerp(transform.position.x, target.position.x, Time.deltaTime * Mathf.Abs(target.transform.position.x - transform.position.x)), 
			                                 Mathf.Lerp(transform.position.y, target.position.y+height, Time.deltaTime * Mathf.Abs(target.transform.position.x - transform.position.x)), 
			                                 Mathf.Lerp(transform.position.z, target.position.z - depthDistance, rate * Time.deltaTime));
		//transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position), rate * Time.fixedDeltaTime);
		//Camera.main.transform.LookAt(target.transform.position);
		//transform.rotation = Quaternion.AngleAxis(((transform.position.x - target.position.x) / levelWidth) * 5.0f, transform.forward);
	}
}