﻿using UnityEngine;
using System.Collections;

public class BaseEnemy : MonoBehaviour {

	
	public const float detectionDistance = float.PositiveInfinity;

	GameObject[] mainPlayers; // make this a list if multiple?
	GameObject closestPlaya;

	public delegate void EventHandler(GameObject e);
	public delegate void InputHandler(GameObject e, float strength);
	public delegate void ButtonHandler(GameObject e, ButtonStates buttonState);
	
	public event InputHandler MoveHorizontal;
	public event InputHandler MoveVertical;
	
	public event InputHandler FireLazors;

	public float turnSpeed = 5.0f;

	Quaternion initalRotation;

	public float shotInterval = 1.0f;

	float timeSinceLastShot = 0.0f;

	// Use this for initialization
	void Start () 
	{
		//find all PLAYAS HAHAHAHAHAHA! ahrm. right.
		mainPlayers = GameObject.FindGameObjectsWithTag("Player");


		initalRotation = transform.rotation;
		timeSinceLastShot = shotInterval;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(timeSinceLastShot < shotInterval)
			timeSinceLastShot += Time.deltaTime;

		if(mainPlayers != null) // are there any players?
		{

			//TODO REPLACE WITH FIND DIRECTION (LEFT / RIGHT; UP / DOWN), trigger event
			closestPlaya = null;
			float shortestDistance = detectionDistance;
			//go through players and get the closest one. (within treshold)
			foreach(GameObject playa in mainPlayers)
			{	
				if(playa != null && Vector3.Distance(playa.transform.position, this.transform.position) < shortestDistance)
				{
					closestPlaya = playa;
				}
			}

			if(closestPlaya != null)
			{
				//found a player who's close!
				Quaternion rotateTowards = Quaternion.LookRotation(Vector3.Normalize(closestPlaya.transform.position - this.gameObject.transform.position));
				//this.transform.LookAt(mainPlayers[closestPlayaIndex].transform.position, Vector3.up); //LOOK AT MAH SHIP
				transform.rotation = Quaternion.Slerp(this.transform.rotation, rotateTowards, Time.deltaTime * turnSpeed);
				// also shoot! BLAM BLAM
				OnFire(1.0f);

			}
			else 
			{
				// turn back to normal
			}
		}

	}

	void OnMoveHorizontal(float strength)
	{
		if(MoveHorizontal != null)
			MoveHorizontal(this.gameObject, strength);
	}
	
	void OnMoveVertical(float strength)
	{
		if(MoveVertical != null)
			MoveVertical(this.gameObject, strength);
	}

	void OnFire(float strength)
	{
		if(timeSinceLastShot >= shotInterval)
		{
			if(FireLazors != null)
				FireLazors(this.gameObject, Mathf.Min(strength, 1.0f));
			timeSinceLastShot = 0.0f;
		}
			
	}
}
