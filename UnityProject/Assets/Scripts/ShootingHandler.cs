﻿using UnityEngine;
using System.Collections;

public class ShootingHandler : MonoBehaviour 
{

	// creates bullet instances. 
	// can be attached to one ship (single gun)
	// or to a gun object attached to a ship (multiple guns)
	public GameObject lazors;
	public float forwardOffset;
	public float minSpeed = 50.0f;
	public float maxSpeed = 100.0f;

	bool isPlayer = false;

	//instantiate bullet with direction at firing objects bullet position.
	// TODO: implement No friendly fire.

	// Use this for initialization
	void Start () 
	{
		if(this.transform.root.CompareTag("Player"))
		{
			isPlayer = true;
			this.gameObject.GetComponent<ShipInputHandler>().FireLazors += FireLazors;
		}
		else if(this.transform.root.CompareTag("Enemy"))
		{
			this.gameObject.GetComponent<BaseEnemy>().FireLazors += FireLazors;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		// TODO: implement firing rate (max per second / interval between shots)
	}

	void FireLazors (GameObject e, float strength)
	{
		// send message to all subweapons to handle firing
		this.BroadcastMessage("FireMahLazor", strength, SendMessageOptions.DontRequireReceiver);


		// instantiate bullet / FIRE MAIN CANNON
		GameObject lazorClone;
		if(lazors != null && isPlayer) //&& lazors.rigidbody != null)
		{
			lazorClone = (GameObject)Instantiate(lazors, e.transform.localPosition + e.transform.forward * forwardOffset, this.gameObject.transform.rotation);
			lazorClone.rigidbody.AddForce(lazorClone.transform.forward * Mathf.Lerp(minSpeed, maxSpeed, strength));
			lazorClone.transform.localScale = Vector3.one * 2;
			//TODO: couple strength with size//effects and damage!

		}
		// TODO: else: exception

		

	}
}
