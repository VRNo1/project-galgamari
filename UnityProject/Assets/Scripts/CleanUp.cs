﻿using UnityEngine;
using System.Collections;

public class CleanUp : MonoBehaviour {

	// Use this for initialization
	Camera mainCamera;

	public float cleanDistance = 20.0f;

	void Start () 
	{
		mainCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(transform.position.z + cleanDistance < mainCamera.transform.position.z)
			Destroy(gameObject);

		//TODO make better clean up method (outside frustum, far away, etc)
	}
}
