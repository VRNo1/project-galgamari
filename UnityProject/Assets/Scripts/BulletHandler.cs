﻿using UnityEngine;
using System.Collections;

/*public enum ShootingThing
{
	None,
	Player,
	Enemy
}*/


public class BulletHandler : MonoBehaviour {

	public float lifetime = 10;

	//ShootingThing shotBy;
	public ParticleSystem hitParticles;

	float currentLife;
	// Use this for initialization
	void Start () 
	{
		currentLife = 0.0f;
		//shotBy = ShootingThing.None; // harms everything
	}

	//TODO collision event.

	// Update is called once per frame
	void Update () 
	{
		if(currentLife >= lifetime)
		{
			//TODO Fade out
			Destroy(gameObject);
		}
		else 
		{
			currentLife += Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		// TODO trigger explosion 
		// replace gameObject with explosion prefab
		// TODO move to gameobject //ONHIT method
		//Destroy(collision.gameObject);
		if(hitParticles != null)
		{
			ParticleSystem hitBullet;
			hitBullet = (ParticleSystem)Instantiate(hitParticles, transform.position, transform.rotation);
		}

		Destroy(gameObject);
	}
}
