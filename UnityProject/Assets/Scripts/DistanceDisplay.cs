﻿using UnityEngine;
using System.Collections;

public class DistanceDisplay : MonoBehaviour {

	Vector3 startPosition;
	Transform mainCameraTransform;

	// Use this for initialization
	void Start () 
	{
		mainCameraTransform = Camera.main.transform;
		startPosition = mainCameraTransform.position;

	}
	
	// Update is called once per frame
	void Update () 
	{
		float distance = Mathf.Abs(mainCameraTransform.position.z - startPosition.z);
		this.gameObject.GetComponent<GUIText>().text = Mathf.FloorToInt(distance).ToString() + "meters";
	}
}
